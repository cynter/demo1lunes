package com.captton.programa;

import java.util.ArrayList;
import java.util.Scanner;

public class Inicio {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//System.out.println("Hello World!");
		
		String name ="Mariano";
		//name ="pepe";		
		//System.out.println(name);
		Boolean esVaron; // defino la variable
		final String MES1 = "Enero"; // declaracion de constante siempre ene mayuscula
		String diaDeSemana ="Martes";
		float resultado;
		
		//listas
		ArrayList<String> alumnos;
		
		alumnos = new ArrayList<String>(); //asi se inicializa la lista alumnos
		
		alumnos.add("Jorge");
		alumnos.add("Esteban");
		alumnos.add("Jose");
		
		// System.out.println(alumnos.get(0));
		
		for(String alu: alumnos) {
			
			System.out.println(alu);
			if(alu.equals("Jorge"))
				System.out.println("Que haces Jorgito!!");
			
		}
		
		//interaccion con la pantalla
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Ingrese su nombre:");
		String nom = sc.nextLine();
		System.out.println("Ingrese su edad:");
		int edad = sc.nextInt();
		
		System.out.println("Tu nombre es: " +nom+ "tu edad es: "+edad);
		
		if(edad>40)
			System.out.println("Que viejo estas!!");
		
		
		
		//vectores
		int[] numeros;
		numeros = new int[5]; //instancia
		
		numeros[0] =24;
		numeros[1] =45;
		numeros[2] =14;
		
		System.out.println("El numero en la pos 1 es:"+numeros[1]);
		
		for(int cont=0; cont <5; cont++)
		{
			System.out.println("El numero es:"+numeros[cont]);
		}
		
		
		resultado = 15f / 2;
		int res2 = (int)resultado;//casteo
		
		System.out.println("El resultado es:" + resultado);
		System.out.println("El resultado2 es:" + res2);
		esVaron = true; // siempre hay que inicializar
		//System.out.println("El valor de esVaron es:" + esVaron);
		
//		//si es una sola linea no se ponen llaves
//		if(esVaron ==true)
//			System.out.println("El valor de esVaron es: true");
//		else
//			System.out.println("El valor de esVaron es: false");
		
		if(esVaron) {
			System.out.println("El valor de esVaron es: true");
			System.out.println("Claro es varon porque la variable era true");
		}
		else {
			System.out.println("El valor de esVaron es: false");
		}
		
		System.out.println("Hola "+name);
		
//		if(name =="Mariano")
//			System.out.println("Que haces Marian!");
		
		if(name.equals("Mariano"))
			System.out.println("Que haces Marian!");
		
		switch(diaDeSemana)
		{
		case "Lunes":
			System.out.println("Hoy es Lunes");
			break;
		case "Martes":
			System.out.println("Hoy es Martes");
			break;
		
		}
		
		
		//estrucuturas repetitivas
		for(int cont=0; cont <=5; cont++)
		{
			//System.out.println("Prueba del contador");
			System.out.println("Prueba del contador"+cont);
		}
		
		
		
		
		
		

	}

}
